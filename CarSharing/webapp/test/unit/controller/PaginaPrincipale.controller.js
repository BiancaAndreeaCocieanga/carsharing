/*global QUnit*/

sap.ui.define([
	"CarSharing/CarSharing/controller/PaginaPrincipale.controller"
], function (Controller) {
	"use strict";

	QUnit.module("PaginaPrincipale Controller");

	QUnit.test("I should test the PaginaPrincipale controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});